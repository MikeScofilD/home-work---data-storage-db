import json

# Задание 1
# Создайте простые словари и сконвертируйте их в JSON. Сохраните JSON в файл и попробуйте
# загрузить данные из файла.

person = {
    "name": "Vasya",
    "last_name": "Pupkin",
    "age": "20",
}

json_person = json.dumps(person)
print("json.dumps->")
print(json_person)

with open('json_person.json', 'w') as f:
    print("json.dumps open ->")
    json.dump(person, f)

# dict_person = json.loads(json_person)
# print(dict_person)

with open('json_person.json', 'r') as f:
    dict_person = json.load(f)
    print(dict_person)
