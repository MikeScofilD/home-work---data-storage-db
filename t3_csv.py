import csv


# Задание 3
# Поработайте с созданием собственных диалектов, произвольно выбирая правила для CSV файлов.
# Зарегистрируйте созданные диалекты и поработайте, используя их, с созданием/чтением файлом.

class Dialect(csv.Dialect):
    quoting = csv.QUOTE_ALL
    quotechar = "="
    delimiter = "-"
    lineterminator = '\n'


csv.register_dialect('di', Dialect)

with open('csv_person.csv', 'w') as f:
    wrt = csv.writer(f, dialect='di')
    wrt.writerow(['Hello', 'world'])

with open('csv_person.csv', 'r') as f:
    read = csv.reader(f, dialect='di')
    for text in read:
        print(text)
