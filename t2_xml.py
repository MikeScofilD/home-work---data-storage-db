from xml.etree import ElementTree as Person

# Задание 2
# Создайте XML файл с вложенными элементами и воспользуйтесь языком поиска XPATH.
# Попробуйте осуществить поиск содержимого по созданному документу XML, усложняя свои
# запросы и добавляя новые элементы, если потребуется.

person = Person.parse('xml_person.xml')
root = person.getroot()

# print(f"person = {person}")
# print(f"root = {root}")

children = root.getchildren()
# print(f"children = {children}")
# прошлись по всему xml
# for person in children:
#    print(f"{person.attrib}")
#    for child in person:
#        print(f"<{child.tag}> -> {child.text}")

print("1) Создаем элемент person")

root = Person.Element('data')
add_element = 'y'
while add_element == 'y' and add_element != 'n':
    person = Person.SubElement(root, 'person')
    person_name = Person.SubElement(person, 'name')
    name = str(input("Введите имя: "))
    person_name.text = name

    person_surname = Person.SubElement(person, 'surname')
    surname = str(input("Введите фамилию: "))
    person_surname.text = surname
    print(f"name:{person_name.text} surname:{person_surname.text}")
    add_element = str(input("Добавить имя? y/n: "))

# print(Person.dump(root))
# Создаем xml файл
xml_save = Person.ElementTree(root)
xml_save.write('new_person.xml', encoding='utf-8')

# Поиск
name = root.find('./person/name').text
print(name)
surname = root.find('./person/surname').text
print(surname)
